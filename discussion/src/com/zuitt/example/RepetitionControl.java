package com.zuitt.example;

public class RepetitionControl {
    public static void main(String[] args){
        // [SECTION] Loops
        // are control Structure that allow code block to be executed multiple times

        // While Loop
        // Allow for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate is indefinite

        // to create the variable that will serve as the basis of our condition
        int x = 0;

        while(x < 10){
            System.out.println("The value of x: " + x);
            x++;
        }
        // Do-While Loop
        // similar to while loop. However, do-while loops always execute at least once - while loops may not executable/run at all
        int y = 11;
        do{
            System.out.println("The current value of y is: " + y);
            y++;
        } while (y < 11);

        //For-loop
            // Syntax: for(initialValue; condition/stopper; iteration){codeblock/Statement}

        for(int i = 1; i < 10; i++){
            System.out.println("The value of i is: " + i);
        }
        // sample for loop for arrays
        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for(int name = 0; name < nameArray.length; name++){
            System.out.println("Name: " + nameArray[name]);
        }

        // different sample for loop for arrays
        for(String name : nameArray){
            System.out.println(name);
        }

        String[][] classrooms = new String[3][3];
        //First row
        classrooms[0][0] = "Athos";
        classrooms[0][1] = "Porthos";
        classrooms[0][2] = "Aramis";
        //Second row
        classrooms[1][0] = "Brandon";
        classrooms[1][1] = "JunJun";
        classrooms[1][2] = "Jobert";
        //Third row
        classrooms[2][0] = "Mickey";
        classrooms[2][1] = "Donald";
        classrooms[2][2] = "Goofy";

        // Using For-Loop iterates all the possible elements in the two-dimensional array.
        // for loop in a multidimensional array
       for(String[] row : classrooms){
            // Add another for loop that will iterate on the nested array inside our classrooms array
           for(String column : row){
               System.out.println(column);
           }
       }

    }
}
